@file:JsModule("date-fns")
@file:JsNonModule

import kotlin.js.Date

external fun format(date: Date, format: String): String