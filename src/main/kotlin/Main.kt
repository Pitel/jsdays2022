import kotlinx.browser.document
import kotlin.js.Date

fun main() {
    document.getElementById("add")?.textContent = "${add(2, 3)}"
    document.getElementById("datefns")?.textContent = format(Date(2022, 1, 17), "d. M. yyy")
}

fun add(a: Int, b: Int) = a + b